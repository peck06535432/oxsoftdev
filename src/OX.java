import java.util.Scanner;

public class OX {
	static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static int row, col;
	static char player = 'X';
	static int turn = 0;
	private static int rowInd;
	private static int colInd;

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();
			if (chackWin()) {
				break;
			} else {
				switchPlayer();
			}
		}
		showTable();
		showWin();
		showBye();

	}

	private static void switchPlayer() {
		player = player == 'X' ? 'O' : 'X';
		turn++;
	}

	private static void showWelcome() {
		System.out.println("Start Game OX");
	}

	private static void showTable() {
		System.out.println("  1 2 3");
		for (int rowInd = 0; rowInd < table.length; rowInd++) {
			System.out.print(rowInd + 1);
			for (int colInd = 0; colInd < table[rowInd].length; colInd++) {
				System.out.print(" " + table[rowInd][colInd]);
			}
			System.out.println();
		}
	}

	private static void showTurn() {
		System.out.println("Turn " + player);
	}

	private static void input() {
		while (true) {
			try {
				Scanner sc = new Scanner(System.in);
				System.out.println("Plz choose position (R,C) : ");
				String input = sc.nextLine();
				String str[] = input.split(" ");
				if (str.length != 2) {
					System.out.println("Error : Plz choose position (R,C) : (Ex. 1 1 )");
					continue;
				}
				row = Integer.parseInt(str[0]);
				col = Integer.parseInt(str[1]);
				if(row>3&&row<1 || col>3 ||col<1) {
					System.out.println("Error : Plz input another (R,C) between 1-3");
					continue;
				}
				if (!setTable()) {
					System.out.println("Error : Plz input another (R,C) ");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Error : Plz choose position (R,C) : (Ex. 1 1 )");
				continue;
			}
		}
	}

	private static boolean setTable() {
		if (table[row - 1][col - 1] != '-') {
			return false;
		}
		table[row - 1][col - 1] = player;
		return true;
	}

	private static boolean chackWin() {
		if (chackRow()) {
			return true;
		} else if (chackCol()) {
			return true;
		} else if (chackX()) {
			return true;
		}
		if (isDraw()) {
			return true;
		}
		return false;
	}

	private static boolean isDraw() {
		if (turn == 8) {
			return true;
		} else {
			return false;
		}

	}

	private static boolean chackX() {
		if (chackX1()) {
			return true;
		} else if (chackX1()) {
			return true;
		} else
			return false;
	}

	private static boolean chackX1() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][i] != player)
				return false;
		}
		return true;
	}

	private static boolean chackX2() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][2 - i] != player)
				return false;
		}
		return true;
	}

	private static boolean chackRow(int rowInd) {
		for (int colInd = 0; colInd < table[rowInd].length; colInd++) {
			if (table[rowInd][colInd] != player)
				return false;
		}
		return true;
	}

	private static boolean chackRow() {
		for (int rowInd = 0; rowInd < table.length; rowInd++) {
			if (chackRow(rowInd))
				return true;
		}
		return false;
	}

	private static boolean chackCol(int colInd) {
		for (int rowInd = 0; rowInd < table[colInd].length; rowInd++) {
			if (table[rowInd][colInd] != player)
				return false;
		}
		return true;
	}

	private static boolean chackCol() {
		for (int colInd = 0; colInd < table[0].length; colInd++) {
			if (chackCol(colInd))
				return true;
		}
		return false;
	}

	private static void showWin() {
		if (isDraw()) {
			System.out.println("Draw");
		} else {
			System.out.println(player + "Win ....");
		}
	}

	private static void showBye() {
		System.out.println("Bye Bye ....");
	}
}
